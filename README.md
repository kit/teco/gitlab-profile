# Technology for Pervasive Computing
>Analytics – Sensors – Interaction

The TECO research group was founded in 1993 at the University of Karlsruhe (now KIT – Karlsruhe Institute of Technology) for R&D in applied telematics in close collaboration with the industry.

Ubiquitous Computing is the cornerstone of work and competence, with
Mobile Computing, HCI, Sensor/Actor Networks (especially Cyber Physical
Systems (CPS), Distributed Collaborative Systems (especially Internet of
Things, IoT)), Sensor Big Data Analytics,  and Context-Sensitive Systems
as current research foci. We are heading Germany’s Big Data Centre, the
[Smart Data Innovation Lab
(SDIL)](https://www.sdil.de/de/home-page "Smart Data Innovation Lab (SDIL)")
and co-heading [Baden-Württembergs Smart Data Solution Center
(SDSC)](https://www.sdsc-bw.de/). TECO is associated with the chair of
[Pervasive Computing Systems](http://pcs.tm.kit.edu/) group of the
[Institute of Telematics](http://www.tm.kit.edu/) at the KIT which
covers the education activities. TECO distinguishes itself in being
close to industry both with respect to its funding model and its
participation in “real-world projects” with a range of partners in the
software and hardware industries and companies that apply pervasive
computing technology (e.g. in the context of
[HealthTechnologies](http://www.healthteco.kit.edu) and Industry 4.0).
Many of our projects results, software and documentation are Open Source
and can be found at [GitHub](https://github.com/teco-kit).
